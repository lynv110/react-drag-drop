import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.scss';
import _ from 'lodash/lodash';
import Mock from './Mock';

class App extends React.Component {
    itemWidth = 100;
    itemHeight = 80;
    startHeight = 0;
    startWidth = 0;
    arrRight = [];
    arrLeft = Mock;

    state = {
        text: 'HELLO WORLD',
        arrLeft: [...this.arrLeft],
        arrRight: [...this.arrRight],
        itemSelected: null,
        bg: {}
    };

    calculatePos = (items) => {
        const self = this;
        items.map(function (item) {
            if (item.isMoving) {
                return item;
            }
            const width = item.w * self.itemWidth;
            const height = item.h * self.itemHeight;
            const top = item.y * self.itemHeight;
            const left = item.x * self.itemWidth;
            item.width = width;
            item.height = height;
            item.top = top;
            item.left = left;
            return item;
        });
        return items;
    };

    handleClick = () => {
        this.setState({
            bg: {
                r: Math.random() * 255,
                g: Math.random() * 255,
                b: Math.random() * 255,
            }
        })
    };

    handleDrop = (evt, itemMoving) => {
        const { arrLeft, arrRight } = this.state;
        const itemDropped = _.findIndex(arrRight, {id: itemMoving.id});
        if (itemDropped > -1) {
            return;
        }

        const index = _.findIndex(arrLeft, {id: itemMoving.id});

        const targetWrap = document.getElementById('target-wrap');
        const targetWrapClientRect = targetWrap.getBoundingClientRect();
        targetWrap.classList.remove('hovering');

        const elItemId = document.getElementById(`item-${itemMoving.id}`);
        if (elItemId) {
            elItemId.classList.remove('moving');
        }
        if (
            evt.clientY > targetWrapClientRect.top
            && evt.clientX > targetWrapClientRect.left
            && evt.clientY < targetWrapClientRect.bottom
            && evt.clientX < targetWrapClientRect.right
        ) {
            arrLeft.splice(index, 1);
            this.setState({
                arrRight: [...arrRight, {...itemMoving}],
            });
        }
        this.setState({
            arrLeft,
        });
    };

    mouseUpHandler = e => {
        const { arrLeft, itemSelected } = this.state;
        const itemMoving = _.find(arrLeft, {id: itemSelected.id});
        itemMoving.isMoving = false;
        this.handleDrop(e, itemMoving);
        window.removeEventListener('mouseup', this.mouseUpHandler);
        window.removeEventListener('mousemove', this.mouseMovingHandler);
    };

    mouseMovingHandler = e => {
        const { arrLeft, itemSelected } = this.state;
        const top = e.clientY;
        const left = e.clientX;
        const itemMoving = _.find(arrLeft, {id: itemSelected.id});
        const elItemId = document.getElementById(`item-${itemSelected.id}`);
        elItemId.classList.add('moving');
        
        const targetWrap = document.getElementById('target-wrap');
        const targetWrapClientRect = targetWrap.getBoundingClientRect();

        if (
            top > targetWrapClientRect.top
            && left > targetWrapClientRect.left
            && top < targetWrapClientRect.bottom
            && left < targetWrapClientRect.right
        ) {
            targetWrap.classList.add('hovering');
        } else {
            targetWrap.classList.remove('hovering');
        }

        itemMoving.top = top - this.startHeight;
        itemMoving.left = left - this.startWidth;
        itemMoving.isMoving = true;
        this.setState({
            arrLeft,
        });
        window.addEventListener('mouseup', this.mouseUpHandler);
    };

    handleMouseDownItem = (pos) => (e) => {
        e.stopPropagation();

        const elItemId = document.getElementById(`item-${pos.id}`);
        const itemClientRect = elItemId.getBoundingClientRect();
        this.startHeight = e.clientY - itemClientRect.top;
        this.startWidth = e.clientX - itemClientRect.left;

        this.setState({
            itemSelected: pos,
        });

        window.addEventListener('mouseup', this.mouseUpHandler);
        window.addEventListener('mousemove', this.mouseMovingHandler);
    };

    handleReset = () => {
        this.setState({
            arrLeft: [...this.arrLeft],
            arrRight: [...this.arrRight],
        });
    };

    render() {
        const { arrLeft, arrRight, bg } = this.state;
        const posLeft = this.calculatePos(arrLeft);
        const posRight = this.calculatePos(arrRight);
        const bgStyle = Object.keys(bg).length ? {
            background: `rgba(${bg.r},${bg.g},${bg.b})`
        }: {};
        return (
            <section className='App'>
                <div className="container">
                    <div className='wrap-container'>
                        <div className='alert alert-primary text-center' style={bgStyle}>{this.state.text}</div>
                        <div className='row'>
                            <div className='col-xs-12 col-sm-6'>
                                <div className='left'>
                                    {
                                        posLeft.map(pos => (
                                            <div className='wrap-item' id={`item-${pos.id}`} onMouseDown={this.handleMouseDownItem(pos)} style={{
                                                width: `${pos.width}px`,
                                                height: `${pos.height}px`,
                                                top: `${pos.top}px`,
                                                left: `${pos.left}px`
                                            }}>
                                                <div className="main-item"><span>Move me</span></div>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                            <div className='col-xs-12 col-sm-6'>
                                <div className='right' id='target-wrap'>
                                    {
                                        posRight.map(pos => (
                                            <div className='wrap-item' style={{
                                                width: `${pos.width}px`,
                                                height: `${pos.height}px`,
                                                top: `${pos.top}px`,
                                                left: `${pos.left}px`
                                            }}>
                                                <div className="main-item"><span>Here me</span></div>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <button type='button' className='btn btn-success' onClick={this.handleReset}>Reset</button>&nbsp;
                        <button type='button' className='btn btn-primary pull-right' onClick={this.handleClick}>Click</button>
                    </div>
                </div>
            </section>
        );
    }
}

export default App;
